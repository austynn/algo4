#include <iostream>
#include "Games.h"

//Constructor and Allocated Array
Game_List::Game_List(int gameSize) {
	size = gameSize;
	numGames = 0;
	gamesArray = new gameInfo[size];
}

//Destructor
Game_List::~Game_List() {
	delete[] gamesArray;
}

//stub functions
//add will add a game and put it into the list
void Game_List::add() {
	gameInfo game;

	cout << "Game Name: ";
	cin.ignore();
	getline(cin, game.name, '\n');

	cout << "Genre: ";
	getline(cin, game.genre, '\n');

	cout << "Amount of players: ";
	cin >> game.playerAmount;

	cin.ignore();
	cout << "Review: ";
	getline(cin, game.review, '\n');

	cout << "Rate the game out of 5: ";
	cin >> game.rating;
	cout << "------------------------------" << endl;

	//store game into the array
	gamesArray[numGames] = game;
	numGames++;
}

//displays the games and reviews
void Game_List::display() {
	gameInfo game;

	cout << "Displaying list: " << endl;
	for (int i = 0; i < numGames; i++) {
	cout << "Game: " << gamesArray[i].name << endl;
	cout << "Genre: " << gamesArray[i].genre << endl;
	cout << "Player Amount: " << gamesArray[i].playerAmount << endl;
	cout << "Rating: " << gamesArray[i].rating << "/5" << endl;

	//when user enters a review, reviewRating will have a randomize number of viewers who dislike or like your review
	cout << "Review: " << gamesArray[i].review << endl;
	if (gamesArray[i].reviewRating > 0) {
		cout << gamesArray[i].reviewRating << " viewers liked this." << endl;
	}
	else if (gamesArray[i].reviewRating < 0) {
		cout << gamesArray[i].reviewRating << " viewers disliked this." << endl;
	}
	cout << "------------------------------" << endl;
	}
}

//Search allows user tp type the genre they are lookng for.
void Game_List::search() {
	string genre;
	cin.ignore();

	cout << "Type the genre of game you are looking for." << endl;
	cout << "Genre: ";
	getline(cin, genre);

	bool found = false;

	cout << "Displaying all related games of that genre." << endl;
	for (int i = 0; i < numGames; i++) {
		if (gamesArray[i].genre == genre) {
			cout << "Game: " << gamesArray[i].name << endl;
			cout << "Genre: " << gamesArray[i].genre << endl;
			cout << "Player Amount: " << gamesArray[i].playerAmount << endl;
			cout << "Rating: " << gamesArray[i].rating << "/5" << endl;
			cout << "Review: " << gamesArray[i].review << endl;
			cout << "------------------------------" <<endl;
			found = true;
		}
	//if genre not found, send error
	}
	if (!found) {
		cout << "Genre not found." << endl;
		cout << endl;
	}
}

//custom function
//user can clear the if they wish
void Game_List::clear() {
	char response;
	bool loop = true;

	cout << "Are you sure you want to clean the list?: ";
	cin >> response;
	cout << endl;

	while (loop) {
		//if yes, clear numGames and break loop
		if (toupper(response) == 'Y') {
			cout << "Clearing the list." << endl;
			numGames = 0;
			cout << "------------------------------" << endl;
			loop = false;
		}
		//if no, do not clear and break loop
		else if (toupper(response) == 'N') {
			cout << "Cancelled." << endl;
			cout << "------------------------------" << endl;
			loop = false;
		}
		else {
			cout << "Incorrect input" << endl;
		}
	}

}	
