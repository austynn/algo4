#include <iostream>
#include <cstring>
using namespace std;

//Structure
struct gameInfo {
string name;
string genre;
int playerAmount = 0;
string review;
//custom features
int rating = 0;
int reviewRating = -20 + (rand() % 60);
};

//class interface
class Game_List {
	public:
		Game_List(int gameSize);
		~Game_List();
		void add();
		void display();
		void search();
		void clear();
		
	private:
		gameInfo* gamesArray;
		int size;
		int numGames;
};
