/* Austyn Ngo
 * CS162
 * 3/7/2024
 *
 * This program will let you review a game by allowing the user to first, create the game. The user can
 * also display all reviews they have for every game they make in the array list and when the user
 * is done, they can quit. There are three files for this, algo4.cpp, Games.cpp, and Games.h.
 * Algo4.cpp is the main file, Games.cpp is where my stub functions are, along with the allocated
 * array and the constructor, and Games.h is my class interface and structures.
 */

#include "Games.h"
#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

int main() {
	char input;
	bool loop = true;
	string genre;
	
	Game_List games(10);	

	//loop function
	while (loop) {
		cout << "Menu: [A]Add, [D]Display, [S]Search, [C]Clear, [Q]Quit" << endl;
		cin >> input;
		cout << endl;

		switch(toupper(input)) {
			//runs add function
			case 'A':
				games.add();
				break;
			//runs display function
			case 'D':
				games.display();
				break;
			//searches based on the genre
			case 'S':
				games.search();
				break;
			//clears the list
			case 'C':
				games.clear();
				break;
			//quits program
			case 'Q':
				cout << "Closing program." << endl;
				loop = false;
				break;
			//error if input is wrong
			default:
				cout << "Invalid input" << endl;
		}
	}

	return 0;
}
